#!/bin/sh
# todo, A command line TODO manager
# Copyright (C) 2008  Alejandro Mery <amery@geeks.cl>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

todo_usage()
{
	local cmd="${0##*/}"

	[ $# -eq 0 ] || echo "ERROR: $*" >&2
	cat <<-EOT >&2
	Usage: $0 [options] [<object>]
	   ${cmd} [-t <tag>*] [<message>]   - new entry
	   ${cmd} (-e|--edit) <hash>        - edit an entry
	   ${cmd} (-T|--tags)               - list tags
	   ${cmd} (-l|--list) [<tag>*]      - list entries
	   ${cmd} -L [<tag>*]               - list entry hashes

	EOT
	exit 1
}

todo_sane_tags()
{
	echo "$*" | tr 'a-z' 'A-Z' | sed \
		-e 's/^ *//' -e 's/ *$//' -e 's/  */ /g'
}

todo_entry()
{
	local hash="$1" hash_1= entry=
	if [ -n "$hash" ]; then
		hash_1=$( echo $hash | cut -c1 )
		entry="$TODODIR/entries/$hash_1/$hash"
		ls -1 "$entry"* 2> /dev/null | head -n 1
	fi
}

trap ':' INT

options=$( getopt -o "t:TLle" -l "tags,list,edit" -- "$@" )
if [ $? -ne 0 ]; then
        todo_usage
fi

# load new arguments list
eval set -- "$options"

TAGS=
MODE=new

while [ $# -gt 0 ]; do
	case "$1" in
	-T|--tags)
		MODE=tags ;;
	-e|--edit)
		MODE=edit ;;
	-l|--list)
		MODE=list ;;
	-L)
		MODE=list_raw ;;

	-t)	TAGS="$TAGS $2"; shift ;;

	--)     shift; break ;;
	-*)	todo_usage unknown option "$1"
		;;
	esac
	shift
done

TODODIR="$HOME/.todo"

todo_new()
{
	TODOTMP="${TMP:-/tmp}/.todo.$$"
	DATE=
	# find a unique date/hash
	#
	while [ -z "$DATE" ]; do
		DATE=$(date --rfc-3339=seconds)
		DATEHASH=$( echo "$DATE" | sha1sum | cut -d' ' -f1 )
		DATEHASH_1=$( echo "$DATEHASH" | cut -c 1 )
		ENTRYDIR="$TODODIR/entries/$DATEHASH_1"
		ENTRY="$ENTRYDIR/$DATEHASH"

		if [ -e "$ENTRY" ]; then
			DATE=
			sleep 1
		else
			mkdir -p "$ENTRYDIR"
		fi
	done

	if [ "$*" != "" ]; then
		echo "Subject: $*" > "$TODOTMP"
		echo "Added: $DATE" >> "$TODOTMP"
	else
		echo "Subject: ..." > "$TODOTMP.empty"
		echo "Added: $DATE" >> "$TODOTMP.empty"
		cp "$TODOTMP.empty" "$TODOTMP"

		${EDITOR:-vi} "$TODOTMP"

		if cmp -s "$TODOTMP" "$TODOTMP.empty" ||
				[ ! -s "$TODOTMP" ]; then
			rm -f "$TODOTMP" "$TODOTMP.empty"
			return
		fi
	fi

	mv "$TODOTMP" "$ENTRY"

	for tag in $(todo_sane_tags $TAGS) INBOX; do
		[ -d "$TODODIR/$tag" ] || mkdir "$TODODIR/$tag"

		ln -s "../entries/$DATEHASH_1/$DATEHASH" "$TODODIR/$tag/$DATEHASH"
	done

	echo "$DATEHASH"
}

todo_tags()
{
	ls -1d "$TODODIR"/* | sed -e "s,^$TODODIR/,," | grep -v '^entries$'
}

todo_list_raw()
{
	local tags="$(todo_sane_tags $TAGS $*)"
	local tag=
	if [ -n "$tags" ]; then
		for tag in $tags; do
			ls -1 "$TODODIR/$tag"/* 
		done
	else
		ls -1 "$TODODIR/entries"/*/*
	fi 2> /dev/null | sed -e 's,.*/,,' | sort -u
}

todo_list()
{
	local hash= short= entry= subject=
	for hash in $( $0 -L $(todo_sane_tags $TAGS $* )); do
		short="$( echo $hash | cut -c1-8 )"
		entry="$( todo_entry $hash )"
		subject=$( grep '^Subject:' "$entry" | head -n 1 |
				cut -d' ' -f2- )

		[ -n "$subject" ] || subject=$( head -n1 "$entry" )
		[ -n "$subject" ] || subject="empty"

		echo "$short $subject"
	done
}

todo_edit_gc()
{
	local entry= hash=
	for entry; do
		if [ ! -s "$entry" ]; then
			hash="${entry##*/}"
			rm -f "$entry" "$TODODIR"/*/"$hash"
		fi
	done
}

todo_edit()
{
	local entries= entry= hash=
	for hash; do
		entry="$(todo_entry $hash)"
		[ -z "$entry" ] || entries="$entries '$entry'"
	done

	[ -n "$entries" ] || return

	eval "${EDITOR:-vi} $entries"

	eval "todo_edit_gc $entries"
}

case "$MODE" in
	new)	todo_new "$*" ;;
	edit)	todo_edit "$@" ;;
	tags)	todo_tags ;;
	list)	todo_list "$*" ;;
	list_raw)	todo_list_raw "$*" ;;
esac
